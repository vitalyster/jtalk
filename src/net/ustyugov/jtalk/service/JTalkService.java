/*
 * Copyright (C) 2012, Igor Ustyugov <igor@ustyugov.net>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/
 */

package net.ustyugov.jtalk.service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;

import net.ustyugov.jtalk.AutoAwayStatus;
import net.ustyugov.jtalk.AutoXaStatus;
import net.ustyugov.jtalk.Avatars;
import net.ustyugov.jtalk.ChangeConnectionReceiver;
import net.ustyugov.jtalk.Conference;
import net.ustyugov.jtalk.Constants;
import net.ustyugov.jtalk.IconPicker;
import net.ustyugov.jtalk.IgnoreList;
import net.ustyugov.jtalk.MessageItem;
import net.ustyugov.jtalk.MessageLog;
import net.ustyugov.jtalk.Notify;
import net.ustyugov.jtalk.activity.RosterActivity;
import net.ustyugov.jtalk.db.AccountDbHelper;
import net.ustyugov.jtalk.db.JTalkProvider;
import net.ustyugov.jtalk.db.WidgetDbHelper;
import net.ustyugov.jtalk.listener.ConListener;
import net.ustyugov.jtalk.listener.IncomingFileListener;
import net.ustyugov.jtalk.listener.InviteListener;
import net.ustyugov.jtalk.listener.LocationChangedListener;
import net.ustyugov.jtalk.listener.MsgListener;
import net.ustyugov.jtalk.listener.MucParticipantStatusListener;
import net.ustyugov.jtalk.listener.RstListener;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.PrivacyListManager;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.ChatState;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.OfflineMessageManager;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.Receipt;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.bookmark.BookmarkManager;
import org.jivesoftware.smackx.bookmark.BookmarkedConference;
import org.jivesoftware.smackx.commands.AdHocCommandManager;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.packet.BobExtension;
import org.jivesoftware.smackx.packet.CapsExtension;
import org.jivesoftware.smackx.packet.CaptchaExtension;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.DataForm;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.MultipleAddresses;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.ReceiptExtension;
import org.jivesoftware.smackx.packet.ReplaceExtension;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.packet.VCard;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.BytestreamsProvider;
import org.jivesoftware.smackx.provider.CapsExtensionProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.IBBProviders;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.VersionProvider;
import org.jivesoftware.smackx.search.UserSearch;
import org.xbill.DNS.Credibility;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.SRVRecord;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;

import com.jtalk2.R;
 
public class JTalkService extends Service {
	private static JTalkService js = new JTalkService();
    private static List<String> collapsedGroups = new ArrayList<String>();
    private static List<String> composeList = new ArrayList<String>();
    private static List<String> messagesList    = new ArrayList<String>();
    private static List<String> mucHighlightsList = new ArrayList<String>();
    private static Hashtable<String, String> textHash = new Hashtable<String, String>();
    private static Hashtable<String, Integer> positionHash = new Hashtable<String, Integer>();
    private static Hashtable<String, Conference> joinedConferences = new Hashtable<String, Conference>();
    private static Hashtable<String, MultiUserChat> conferencesHash = new Hashtable<String, MultiUserChat>();
    private static Hashtable<String, Bitmap> avatarsHash = new Hashtable<String, Bitmap>();
    private static Hashtable<String, List<MessageItem>> messagesHash = new Hashtable<String, List<MessageItem>>();
    private static Hashtable<String, List<MessageItem>> mucMessagesHash = new Hashtable<String, List<MessageItem>>();
    private static Hashtable<String, Integer> messagesCount = new Hashtable<String, Integer>();
    private static Hashtable<String, DataForm> formHash = new Hashtable<String, DataForm>(); 
    private Roster roster = null;
    private String currentJid = "me";
    private String sidebarMode = "users";
    private String state = "";
    private Bitmap avatar = null;
    private SharedPreferences prefs;
    private XMPPConnection connection = null;
    private PacketListener msgListener;
    private ConnectionListener conListener;
    private BroadcastReceiver updateReceiver;
    private ChangeConnectionReceiver connectionReceiver;
    private boolean started = false;
    private VCard vCard = null;
    private FileTransferManager fileTransferManager;
    private List<FileTransferRequest> incomingRequests = new ArrayList<FileTransferRequest>();
    private Timer autoStatusTimer = new Timer();
    private boolean autoStatus = false;
    private Presence oldPresence;
    
    private WifiManager wifiManager;
    private WifiManager.WifiLock wifiLock;
    
    private LocationManager locationManager;
    private LocationChangedListener locationListener = new LocationChangedListener();

    private ConnectionTask connectionATask = new ConnectionTask();
    private IconPicker iconPicker;

    public static JTalkService getInstance() { return js; }
    
    public int getMessagesCount() {
    	int result = 0;
    	Collection<Integer> values = messagesCount.values();
    	for (Integer val: values) {
    		result = result + val;
    	}
    	return result;
    }
    
    public int getMessagesCount(String jid) {
    	if (messagesCount.containsKey(jid)) return messagesCount.get(jid);
    	else return 0;
    }
    
    public void addMessagesCount(String jid) {
    	if (messagesCount.containsKey(jid)) {
    		messagesCount.put(jid, messagesCount.get(jid).intValue() + 1);
    	} else {
    		messagesCount.put(jid, 1);
    	}
    	updateWidget();
    }
    
    public void removeMessagesCount(String jid) {
    	if (messagesCount.containsKey(jid)) messagesCount.remove(jid);
    	updateWidget();
    }
    
    public void addDataForm(String id, DataForm form) {
    	formHash.put(id, form);
    }
    
    public DataForm getDataForm(String id) {
    	if (formHash.containsKey(id)) return formHash.remove(id);
    	else return null;
    }
    
    public void addLastPosition(String jid, int position) {
    	positionHash.put(jid, position);
    }
    
    public int getLastPosition(String jid) {
    	if (positionHash.containsKey(jid)) return positionHash.remove(jid);
    	else return -1;
    }
    
    public IconPicker getIconPicker() { return iconPicker; }
    public String getSidebarMode() { return sidebarMode; }
    public void setSidebarMode(String mode) { this.sidebarMode = mode; }
    public void setAutoStatus(boolean auto) { this.autoStatus = auto; }
    public boolean getAutoStatus() { return autoStatus; }
    public void setOldPresence(Presence presence) { this.oldPresence = presence; }
    public FileTransferManager getFileTransferManager() { return fileTransferManager; }
    public List<FileTransferRequest> getIncomingRequests() { return incomingRequests; }
    public void setCurrentJid(String jid) { this.currentJid = jid; }
    public String getCurrentJid() { return currentJid; }
    public boolean isStarted() { return started; }
    public String getState() { return state; }
    public void setState(String s) { state = s; }
    public XMPPConnection getConnection() { return connection; }
    public ConnectionTask getConnectionTask() { return connectionATask; }
    public Roster getRoster() { return roster; }
    public Bitmap getAvatar() { return avatar; }
    public List<String> getCollapsedGroups() { return collapsedGroups; }
    public List<String> getComposeList() { return composeList; }
    public Hashtable<String, MultiUserChat> getConferencesHash() { return conferencesHash; }
    public Hashtable<String, Conference> getJoinedConferences() { return joinedConferences; }
    public Hashtable<String, Bitmap> getAvatarsHash() { return avatarsHash; }
    public List<String> getMessagesList() { return messagesList; }
    public boolean isHighlight(String jid) { return mucHighlightsList.contains(jid); }
    public void removeHighlight(String jid) { while (mucHighlightsList.contains(jid)) mucHighlightsList.remove(jid); }
    public void addHighlight(String jid) { mucHighlightsList.add(jid); }
    public Hashtable<String, List<MessageItem>> getMessagesHash() { return messagesHash; }
    public Hashtable<String, List<MessageItem>> getMucMessagesHash() { return mucMessagesHash; }
    public void setText(String jid, String text) { textHash.put(jid, text); }
    public String getText(String jid) {
    	if (textHash.containsKey(jid)) return textHash.get(jid);
    	else return "";
    }
    public VCard getVCard() { return vCard; }
    public void setVCard(VCard vcard) { 
    	if (vcard != null) {
    		vCard = vcard;
        	final byte[] buffer = vCard.getAvatar();
    		if (buffer != null) {
    			avatar = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(buffer, 0, buffer.length), 48, 48, true);
    			new Thread() {
    				public void run() {
    					try {
    						File f = new File(Constants.PATH);
    						f.mkdirs();
    						FileOutputStream fos = new FileOutputStream(Constants.PATH + "/avatar");
    						fos.write(buffer);
    						fos.close();
    					} catch (Throwable t) { }
    				}
    			}.start();
    		} else {
    			avatar = null;
    		}
    		Intent intent = new Intent(Constants.UPDATE);
    		sendBroadcast(intent);
    	}
    }
    
    public Presence getPresence(String user) {
    	Presence unavailable = new Presence(Presence.Type.unavailable);
    	
    	if (!isAuthenticated()) return unavailable;
    	
		if (StringUtils.parseResource(user).length() > 0) {
			String g = StringUtils.parseBareAddress(user);
			if (conferencesHash.containsKey(g)) {
				Presence p = conferencesHash.get(g).getOccupantPresence(user);
				if (p != null) return p;
				else return unavailable;
			}
		} else {
	    	Iterator<Presence> it = roster.getPresences(user);
	    	if(it.hasNext()) {
	    		return it.next();
	    	} else {
	    		return unavailable;
	    	}
		}
		return unavailable;
    }
    
    public Presence.Type getType(String user) {
		if (StringUtils.parseResource(user).length() > 0) {
			String g = StringUtils.parseBareAddress(user);
			if (conferencesHash.containsKey(g)) {
				Presence p = conferencesHash.get(g).getOccupantPresence(user);
				if (p != null) return p.getType();
				else return Presence.Type.unavailable;
			}
		} else {
	    	Iterator<Presence> it = roster.getPresences(user);
	    	if(it.hasNext()) {
	    		return it.next().getType();
	    	} else {
	    		return Presence.Type.unavailable;
	    	}
		}
		return Presence.Type.unavailable;
    }
    
    public Presence.Mode getMode(String user) {
		if (StringUtils.parseResource(user).length() > 0) {
			String g = StringUtils.parseBareAddress(user);
			if (conferencesHash.containsKey(g)) {
				Presence p = conferencesHash.get(g).getOccupantPresence(user);
				if (p != null) {
					Presence.Mode m = p.getMode();
					if (m == null) return Presence.Mode.available;
					else return m;
				}
				else return Presence.Mode.available;
			}
		} 
    	
    	Iterator<Presence> it = roster.getPresences(user);
    	if(it.hasNext()) {
    		Presence presence = it.next();
    		if (presence.getType() != Presence.Type.unavailable) return presence.getMode();
    		else return Presence.Mode.available;
    	}
    	return Presence.Mode.available;
    }
    
    public String getStatus(String user) {
		if (StringUtils.parseResource(user).length() > 0) {
			String g = StringUtils.parseBareAddress(user);
			if (conferencesHash.containsKey(g)) {
				Presence p = conferencesHash.get(g).getOccupantPresence(user);
				if (p != null) {
					String s = p.getStatus();
					if (s == null) return "";
					else return s;
				}
				else return "";
			}
		} 
    	
    	List<String> list = new ArrayList<String>();
    	Iterator<Presence> it = roster.getPresences(user);
    	while(it.hasNext()) {
    		Presence presence = it.next();
    		if (presence.getType() != Presence.Type.unavailable)
    			if (presence.getStatus() == null) list.add("");
    			else list.add(presence.getStatus());
    	}
    	
    	if (!list.isEmpty()) return list.get(0);
    	else return "";
    }
    
    public String getNode(String user) {
		if (StringUtils.parseResource(user).length() > 0) {
			String g = StringUtils.parseBareAddress(user);
			if (conferencesHash.containsKey(g)) {
				Presence p = conferencesHash.get(g).getOccupantPresence(user);
				if (p != null) {
					CapsExtension caps = (CapsExtension) p.getExtension(CapsExtension.NODE_NAME, CapsExtension.XMLNS);
					if (caps != null) return caps.getNode();
					else return null;
				}
				else return null;
			}
		} 
    	
    	List<String> list = new ArrayList<String>();
    	Iterator<Presence> it = roster.getPresences(user);
    	while(it.hasNext()) {
    		Presence presence = it.next();
    		if (presence.getType() != Presence.Type.unavailable) {
    			CapsExtension caps = (CapsExtension) presence.getExtension(CapsExtension.NODE_NAME, CapsExtension.XMLNS);
				if (caps != null) list.add(caps.getNode());
    		}
    	}
    	
    	if (!list.isEmpty()) return list.get(0);
    	else return null;
    }
    
    public LocationChangedListener getLocationListener() { return locationListener; }
    
    public void resetTimer() {
    	if (prefs != null) {
    		autoStatusTimer.purge();
            autoStatusTimer.cancel();
            if (autoStatus) {
            	autoStatus = false;
            	sendPresence(oldPresence.getStatus(), oldPresence.getMode().name(), oldPresence.getPriority());
            }
            autoStatusTimer = new Timer();
            int delayAway = 10;
            int delayXa = 20;
            try {
            	delayAway = Integer.parseInt(prefs.getString("AutoStatusAway", "10"));
                delayXa = Integer.parseInt(prefs.getString("AutoStatusXa", "20"));
            } catch(NumberFormatException e) { }
            if (delayAway < 1) delayAway = 1;
            if (delayXa < delayAway) delayXa = delayAway + 1;
            autoStatusTimer.schedule(new AutoAwayStatus(), delayAway * 60000);
            autoStatusTimer.schedule(new AutoXaStatus(), delayXa * 60000);
    	}
    }
    
    public void updateWidget() {
		int	count = 0;
		
		String[] statusArray = getResources().getStringArray(R.array.statusArray);	
		String status = getString(R.string.NotConnected);
		if (isAuthenticated()) {
			status = statusArray[prefs.getInt("currentSelection", 0)];
			count = getMessagesCount();
		}
		
		ContentValues values = new ContentValues();
        values.put(WidgetDbHelper.MODE, status);
        values.put(WidgetDbHelper.COUNTER, count + "");
        getContentResolver().update(JTalkProvider.WIDGET_URI, values, null, null);
        
        sendBroadcast(new Intent(Constants.WIDGET_UPDATE));
    }
    
    @Override
    public void onCreate() {
    	configure();
    	js = this;
    	prefs = PreferenceManager.getDefaultSharedPreferences(this);
    	conListener = new ConListener(this);
        msgListener = new MsgListener(this);
        iconPicker = new IconPicker(this);
        
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        
        updateReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				updateWidget();
			}
        };
        registerReceiver(updateReceiver, new IntentFilter(Constants.UPDATE));
        
        connectionReceiver = new ChangeConnectionReceiver();
        registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        
        Intent i = new Intent(this, RosterActivity.class);
   		i.setAction(Intent.ACTION_MAIN);
   		i.addCategory(Intent.CATEGORY_LAUNCHER);
   		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
   		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, i, 0);
   		
   		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.stat_offline);
        mBuilder.setContentTitle(getString(R.string.app_name));
        mBuilder.setContentIntent(contentIntent);
    		
		startForeground(1, mBuilder.build());
		
		wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		wifiLock = wifiManager.createWifiLock("jTalk");
    }

    @Override
	public IBinder onBind(Intent intent) {
	    return null;
	}

	@Override
    public void onStart(Intent intent, int startId) {
        started = true;
    }

    @Override
    public void onDestroy() {
    	stopForeground(true);
    	started = false;
    	
    	try {
    		unregisterReceiver(updateReceiver);
    		unregisterReceiver(connectionReceiver);
    	} catch(Exception e) { }
        
        Notify.cancelAll(this);
        clearAll();
        updateWidget();
    }

    public void disconnect(final boolean exit) {
    	if (wifiLock.isHeld()) wifiLock.release();
    	locationManager.removeUpdates(locationListener);
    	
    	connectionATask.cancel(true);
    	if (isAuthenticated()) {
    		leaveAllRooms();
    		connection.removeConnectionListener(conListener);
			Presence presence = new Presence(Presence.Type.unavailable, "", 0, null);
			connection.disconnect(presence);
    	} else if (connection != null && connection.isConnected()) connection.disconnect();
    	if (exit) stopSelf();
    }
    
    public void reconnect() {
    	state = getResources().getString(R.string.Reconnecting) + "...";
    	Intent i = new Intent(Constants.CONNECTION_STATE);
    	sendBroadcast(i);
    	new Thread() {
    		@Override
    		public void run() {
    			disconnect(false);
    			connect();
    		}
    	}.start();
    }
    
    private void createConnectionTask() {
    	connectionATask = new ConnectionTask();
    }
    
    public void connect() {
    	if (prefs.getBoolean("WifiLock", false)) wifiLock.acquire();
    	
		String text  = prefs.getString("currentStatus", "");
		String mode  = prefs.getString("currentMode", "available");
		
		if (mode.equals("online")) {
			mode = "available";
			setPreference("currentSelection", 0);
		}
		
		if (started && !mode.equals("unavailable")) {
			state = getString(R.string.Connecting) + "...";
	    	Intent i = new Intent(Constants.CONNECTION_STATE);
	    	sendBroadcast(i);
	    	
	    	createConnectionTask();
        	connectionATask.execute();
		} else {
			state = text;
			Intent i = new Intent(Constants.CONNECTION_STATE);
	    	sendBroadcast(i);
		}
    }

	public Boolean joinRoom(final String group, final String nick, final String password) {	
		new Thread() {
			@Override
			public void run() {
				if (conferencesHash.containsKey(group)) {
					conferencesHash.remove(group);
				}
				
				MultiUserChat muc = new MultiUserChat(connection, group);
				conferencesHash.put(group, muc);
				
				Presence presence = new Presence(Presence.Type.available);
				presence.setStatus(prefs.getString("currentStatus", ""));
				presence.setMode(Presence.Mode.valueOf(prefs.getString("currentMode", "available")));
				
				try { 
					DiscussionHistory h = new DiscussionHistory();
					h.setMaxStanzas(10);
					muc.addParticipantStatusListener(new MucParticipantStatusListener(group));
					muc.addParticipantListener(new PacketListener() {
						@Override
						public void processPacket(Packet packet) {
							Presence p = (Presence) packet;
					        if (p.isAvailable()) sendBroadcast(new Intent(Constants.PRESENCE_CHANGED));
						}
					});
					muc.join(nick, password, h, 5000, presence);
				}
			  	catch (Exception e) {
			  		Intent eIntent = new Intent(Constants.ERROR);
			  		eIntent.putExtra("error", "Error: " + e.getLocalizedMessage());
			  		sendBroadcast(eIntent);
			  	}
			  		
				if (muc.isJoined()) {
					Conference conf = new Conference(group, nick, password);
					if (!joinedConferences.contains(conf)) joinedConferences.put(group, conf);
					Intent updateIntent = new Intent(Constants.PRESENCE_CHANGED);
					updateIntent.putExtra("join", true);
					updateIntent.putExtra("group", group);
					sendBroadcast(updateIntent);
					
					if (prefs.getBoolean("LoadAllAvatars", false)) {
						Avatars.loadAllAvatars(group);
					}
				}
			}
		}.start();
		
		return true;
	}
	
	public void leaveRoom(String group) {
		if (conferencesHash.containsKey(group)) {
			try {
				MultiUserChat muc = getConferencesHash().get(group);
				if (muc.isJoined()) muc.leave();
			} catch (IllegalStateException ise) { }
			conferencesHash.remove(group);
	    }
	    if (getMucMessagesHash().containsKey(group)) mucMessagesHash.remove(group);
	    if (joinedConferences.containsKey(group)) joinedConferences.remove(group);
	    Intent updateIntent = new Intent(Constants.PRESENCE_CHANGED);
		sendBroadcast(updateIntent);
	}
	
	public void leaveAllRooms() {
		if (!conferencesHash.isEmpty()) {
			Collection<MultiUserChat> coll = conferencesHash.values();
			for(MultiUserChat muc : coll) {
				try {
					if (muc.isJoined()) muc.leave();
				} catch (IllegalStateException ise) { }
			}
			mucMessagesHash.clear();
//			joinedConferences.clear();
//			conferencesHash.clear();
		}
	}
	
    public void addContact(String jid, String name, String group) {
	    try {
		    final String[] groups = { group };
		    roster.createEntry(jid, name, groups);
	    } catch (XMPPException e) {    }
    }
  
    public void removeContact(String jid) {
    	try {
    		RosterEntry entry = roster.getEntry(jid);
    		roster.removeEntry(entry);
    	} catch (XMPPException e) {  }
    }

  	public boolean isAuthenticated() {
  		if (connection != null && connection.getUser() != null && connection.isAuthenticated()) return true;
  		else return false;
  	}

  	public void sendMessage(String user, String message) {
  		sendMessage(user, message, null);
  	}
  	
  	public void sendMessage(String user, String message, String resource) {
  		String mil = System.currentTimeMillis()+"";
  		
  		final Message msg;
  		if (resource != null && resource.length() > 0) msg = new Message(user + "/" + resource, Message.Type.chat);
  		else msg = new Message(user, Message.Type.chat);
  		msg.setPacketID(mil);
  		msg.setBody(message);
  		
  		ReceiptExtension receipt = new ReceiptExtension(Receipt.request);
  		msg.addExtension(receipt);
  		
  		Date date = new java.util.Date();
        date.setTime(Long.parseLong(mil));
        String time = DateFormat.getTimeFormat(this).format(date);
        
  		MessageItem msgItem = new MessageItem();
  		msgItem.setTime(time);
  		msgItem.setName(getResources().getString(R.string.Me));
  		msgItem.setId(mil);
  		msgItem.setBody(message);
  		msgItem.setReceived(false);
  		
  		if (resource != null && resource.length() > 0) MessageLog.writeMessage(StringUtils.parseBareAddress(user) + "/" + resource, msgItem);
  		else MessageLog.writeMessage(user, msgItem);
  		
  		if (messagesHash.containsKey(user)) {
			List<MessageItem> list = messagesHash.get(user);
			list.add(msgItem);
		} else {
			List<MessageItem> list = new ArrayList<MessageItem>();
			list.add(msgItem);
			messagesHash.put(user, list);
		}
  		
  		new Thread() {
  			@Override
  			public void run() {
  				if(connection != null && connection.getUser() != null) {
  					connection.sendPacket(msg);
  				}
  			}
  		}.start();
  	}
  	
  	public void editMessage(final String user, final String id, final String message) {
  		String mil = System.currentTimeMillis()+"";
  		
  		final Message msg = new Message(user, Message.Type.chat);
  		msg.setPacketID(mil);
  		msg.setBody(message);
  		
  		ReplaceExtension replace = new ReplaceExtension(id);
  		msg.addExtension(replace);
  		
  		if(connection != null && connection.getUser() != null) {
				connection.sendPacket(msg);
				MessageLog.editMessage(user, id, message);
		}
  	}
  	
  	public void sendPacket(final Packet packet) {
  		new Thread() {
  			@Override
  			public void run() {
  				if(connection != null && connection.getUser() != null) {
  					connection.sendPacket(packet);
  				}
  			}
  		}.start();
  	}
  	
  	public void sendReceivedPacket(String user, String id) {
  		ReceiptExtension extension = new ReceiptExtension(Receipt.received);
  		final Message msg = new Message(user);
  		msg.setPacketID(id);
  		msg.addExtension(extension);
  		new Thread() {
  			@Override
  			public void run() {
  				if(connection != null && connection.getUser() != null) {
  					connection.sendPacket(msg);
  				}
  			}
  		}.start();
  	}

  	public void setChatState(String user, ChatState state) {
  		if (user != null && getType(user) != Presence.Type.unavailable) {
  			ChatStateExtension extension = new ChatStateExtension(state);
  	  		final Message msg = new Message(user, Message.Type.chat);
  	        msg.addExtension(extension);
  	        new Thread() {
  	  			@Override
  	  			public void run() {
  	  				if(connection != null && connection.getUser() != null) {
  	  					connection.sendPacket(msg);
  	  				}
  	  			}
  	  		}.start();
  		}
  	}
  	
  	public void sendPresence(final String state, final String mode, final int priority) {
  		new Thread() {
  			public void run() {
  				setPreference("currentPriority", priority);
       			setPreference("currentMode", mode);
       			setPreference("currentStatus", state);
				setPreference("currentSelection", getPosition(mode));
				setPreference("lastStatus"+mode, state);
					
  				if (isAuthenticated()) {
  					if (!mode.equals("unavailable")) {
  						Presence presence = new Presence(Presence.Type.available);
  	  					if (state != null) presence.setStatus(state);
  	  					presence.setMode(Presence.Mode.valueOf(mode));
  	  					presence.setPriority(priority);
  	  					connection.sendPacket(presence);
  	  					
  	  					Enumeration<String> e = conferencesHash.keys();
  	  					while(e.hasMoreElements()) {
  	  						try {
	  							MultiUserChat muc = conferencesHash.get(e.nextElement());
	  							if (muc.isJoined())	muc.changeAvailabilityStatus(state, Presence.Mode.valueOf(mode));
	  						} catch (IllegalStateException ise) { }
  	  					}
  	  					
  	  					Notify.updateNotify();
  					} else {
  						connection.removeConnectionListener(conListener);
  						Presence presence = new Presence(Presence.Type.unavailable, state, priority, null);
  						connection.disconnect(presence);
  						Notify.offlineNotify(state);
  					}
  				} else {
  					if (mode.equals("unavailable")) {
  						if (connection != null) {
  							connection.removeConnectionListener(conListener);
  							if (isAuthenticated()) {
  								Presence presence = new Presence(Presence.Type.unavailable, state, priority, null);
  	  	  						connection.disconnect(presence);
  							}
  							connectionATask.cancel(true);
  						}
  						Notify.offlineNotify(state);
  					}
  				}
  				
				Intent i = new Intent(Constants.CONNECTION_STATE);
	            sendBroadcast(i);
	            	
	            i = new Intent(Constants.UPDATE);
	            sendBroadcast(i);
  			}
  		}.start();
  	}
  	
  	public void sendPresenceTo(final String to, final String state, final String mode, final int priority) {
  		new Thread() {
  			public void run() {
  				if (connection.getUser() != null) {
  					Presence presence;
  					
  					if (!mode.equals("unavailable")) {
  						presence = new Presence(Presence.Type.available);
  						presence.setMode(Presence.Mode.valueOf(mode));
  					}
  					else presence = new Presence(Presence.Type.unavailable);
  	  				if (state != null) presence.setStatus(state);
  	  				presence.setTo(to);
  	  				presence.setPriority(priority);
  	  				connection.sendPacket(presence);
  				}
  			}
  		}.start();
  	}
  	
  	public void sendLocation(final Location location) {
		if (started && isAuthenticated()) {
			IQ iq = new IQ() {
				public String getChildElementXML() {
					StringBuilder sb = new StringBuilder();
					sb.append("<pubsub xmlns='http://jabber.org/protocol/pubsub'>");
					sb.append("<publish node='http://jabber.org/protocol/geoloc'>");
					sb.append("<item><geoloc xmlns='http://jabber.org/protocol/geoloc'>");
					if (location != null) {
						final float accuracy = location.getAccuracy();
						final double lat = location.getLatitude();
						final double lon = location.getLongitude();
						
						Bundle extras = location.getExtras();
						for (String s : extras.keySet()) {
							Log.i("Location", s);
						}
						
						sb.append("<accuracy>" + accuracy + "</accuracy>");
						sb.append("<lat>" + lat + "</lat>");
						sb.append("<lon>" + lon +"</lon>");
					}
					sb.append("</geoloc></item></publish></pubsub>");
	   				return sb.toString();
	            }
			};
			iq.setPacketID(System.currentTimeMillis()+"");
			iq.setType(IQ.Type.SET);
			connection.sendPacket(iq);
		}
	}

  	public int getPosition(String mode) {
  		int pos;
  		if (mode.equals("available"))
  			pos = 0;
  		else if (mode.equals("away"))
  			pos = 1;
  		else if (mode.equals("xa"))
  			pos = 2;
  		else if (mode.equals("dnd"))
  			pos = 3;
  		else if (mode.equals("chat"))
  			pos = 4;
  		else 
  			pos = 0;
  		return pos;
  	}
  	
  	public String getCurrentMode() {
  		String mode = null;

  		switch(prefs.getInt("currentSelection", Constants.STATUS_OFFLINE)) {
  		case Constants.STATUS_ONLINE:
  			mode = "available";
  			break;
  		case Constants.STATUS_AWAY:
  			mode = "away";
  			break;
  		case Constants.STATUS_E_AWAY:
  			mode = "xa";
  			break;
  		case Constants.STATUS_DND:
  			mode = "dnd";
  			break;
  		case Constants.STATUS_FREE:
  			mode = "chat";
  			break;
  		case Constants.STATUS_OFFLINE:
  			mode = "unavailable";
  			break;
  		}
  		return mode;
  	}
  
  	public void setPreference(String name, Object value) {
  		SharedPreferences.Editor editor = prefs.edit();
  		if(value instanceof String) editor.putString(name, String.valueOf(value));
  		else if(value instanceof Integer) editor.putInt(name, Integer.parseInt(String.valueOf(value)));
  		else if(value instanceof Boolean) editor.putBoolean(name, (Boolean)value);
  		editor.commit();
  	}
  	
  	private void clearAll() {
  		connectionATask.cancel(true);
  		autoStatusTimer.cancel();
        collapsedGroups.clear();
        composeList.clear();
        messagesList.clear();
        messagesHash.clear();
        mucMessagesHash.clear();
        conferencesHash.clear();
        joinedConferences.clear();
        avatarsHash.clear();
        textHash.clear();
        messagesCount.clear();
        avatar = null;
        vCard = null;
  	}
  	
  	public void configure() {
  		ProviderManager pm = ProviderManager.getInstance();
  		
  		pm.addIQProvider("query","jabber:iq:private", new PrivateDataManager.PrivateDataIQProvider());
  		pm.addIQProvider("query", "jabber:iq:version", new VersionProvider());
        
         //  Roster Exchange
         pm.addExtensionProvider("x","jabber:x:roster", new RosterExchangeProvider());
  
         //  Caps
         pm.addExtensionProvider("c", CapsExtension.XMLNS, new CapsExtensionProvider());
         
         // Messages Receipts
   		 pm.addExtensionProvider("request","urn:xmpp:receipts", new ReceiptExtension.Provider());
   		 pm.addExtensionProvider("received","urn:xmpp:receipts", new ReceiptExtension.Provider());
   		 
   		 // Last Message Correction
   		 pm.addExtensionProvider("replace", "urn:xmpp:message-correct:0", new ReplaceExtension.Provider());
   		
   		 // Captcha
   		 pm.addExtensionProvider("captcha", "urn:xmpp:captcha", new CaptchaExtension.Provider());
   		 pm.addExtensionProvider("data", "urn:xmpp:bob", new BobExtension.Provider());
   		 
         //  Chat State
         pm.addExtensionProvider("active","http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
         pm.addExtensionProvider("composing","http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
         pm.addExtensionProvider("paused","http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
         pm.addExtensionProvider("inactive","http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
         pm.addExtensionProvider("gone","http://jabber.org/protocol/chatstates", new ChatStateExtension.Provider());
  
         //  Group Chat Invitations
         pm.addExtensionProvider("x","jabber:x:conference", new GroupChatInvitation.Provider());
  
         //  Service Discovery # Items    
         pm.addIQProvider("query","http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());
         pm.addIQProvider("query","http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());
  
         //  Data Forms
         pm.addExtensionProvider("x","jabber:x:data", new DataFormProvider());
  
         //  MUC User
         pm.addExtensionProvider("x","http://jabber.org/protocol/muc#user", new MUCUserProvider());
  
         //  MUC Admin    
         pm.addIQProvider("query","http://jabber.org/protocol/muc#admin", new MUCAdminProvider());
  
         //  MUC Owner    
         pm.addIQProvider("query","http://jabber.org/protocol/muc#owner", new MUCOwnerProvider());
  
         //  Delayed Delivery
         pm.addExtensionProvider("x","jabber:x:delay", new DelayInformationProvider());
  
         //  VCard
         pm.addIQProvider("vCard","vcard-temp", new VCardProvider());
  
         //  Offline Message Requests
         pm.addIQProvider("offline","http://jabber.org/protocol/offline", new OfflineMessageRequest.Provider());
  
         //  Offline Message Indicator
         pm.addExtensionProvider("offline","http://jabber.org/protocol/offline", new OfflineMessageInfo.Provider());
  
         //  Last Activity
         pm.addIQProvider("query","jabber:iq:last", new LastActivity.Provider());
  
         //  User Search
         pm.addIQProvider("query","jabber:iq:search", new UserSearch.Provider());
  
         //  SharedGroupsInfo
         pm.addIQProvider("sharedgroup","http://www.jivesoftware.org/protocol/sharedgroup", new SharedGroupsInfo.Provider());
  
         //  JEP-33: Extended Stanza Addressing
         pm.addExtensionProvider("addresses","http://jabber.org/protocol/address", new MultipleAddresses.Provider());
  
         //   FileTransfer
         pm.addIQProvider("si","http://jabber.org/protocol/si", new StreamInitiationProvider());
         pm.addIQProvider("query","http://jabber.org/protocol/bytestreams", new BytestreamsProvider());
         pm.addIQProvider("open","http://jabber.org/protocol/ibb", new IBBProviders.Open());
         pm.addIQProvider("close","http://jabber.org/protocol/ibb", new IBBProviders.Close());
         pm.addExtensionProvider("data","http://jabber.org/protocol/ibb", new IBBProviders.Data());
  
         //  Privacy
         pm.addIQProvider("query","jabber:iq:privacy", new PrivacyProvider());
         pm.addIQProvider("command", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider());
         pm.addExtensionProvider("malformed-action", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.MalformedActionError());
         pm.addExtensionProvider("bad-locale", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.BadLocaleError());
         pm.addExtensionProvider("bad-payload", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.BadPayloadError());
         pm.addExtensionProvider("bad-sessionid", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.BadSessionIDError());
         pm.addExtensionProvider("session-expired", "http://jabber.org/protocol/commands", new AdHocCommandDataProvider.SessionExpiredError());
  	}
  	
  	public class ConnectionTask extends AsyncTask<String, Integer, Integer> {
  		String username = null;
	    String password = null;
	    String resource = null;
	    String service = null;
	    String tls = "1";
	    String sasl = "1";
	    int port = 0;
  		Intent intent = new Intent(Constants.CONNECTION_STATE);
  		
  		@Override
  		protected void onPreExecute() {
  			state = getString(R.string.Connecting) + "...";
            sendBroadcast(intent);
            Notify.offlineNotify(getString(R.string.Connecting));
            int activeAccount = prefs.getInt("Account", -1);
  	    	Cursor cursor = null;
  	    	if (activeAccount >= 0) {
  	    		cursor = getContentResolver().query(JTalkProvider.ACCOUNT_URI, null, "_id = '" + activeAccount + "'", null, null);
  	    	}
  	    	else {
  	    		cursor = getContentResolver().query(JTalkProvider.ACCOUNT_URI, null, null, null, AccountDbHelper._ID);
  	    	}
  	    	
  			if (cursor != null && cursor.getCount() > 0) {
  				cursor.moveToFirst();
  				username = cursor.getString(cursor.getColumnIndex(AccountDbHelper.JID)).trim();
  				password = cursor.getString(cursor.getColumnIndex(AccountDbHelper.PASS)).trim();
  				resource = cursor.getString(cursor.getColumnIndex(AccountDbHelper.RESOURCE)).trim();
  				service = cursor.getString(cursor.getColumnIndex(AccountDbHelper.SERVER));
  				
  				try {
  					port = Integer.parseInt(cursor.getString(cursor.getColumnIndex(AccountDbHelper.PORT)));
  				} catch (NumberFormatException nfc) { }
  				
  				tls = cursor.getString(cursor.getColumnIndex(AccountDbHelper.TLS));
  				sasl = cursor.getString(cursor.getColumnIndex(AccountDbHelper.SASL));
  				cursor.close();
  			}
  		}
  		
  		@Override
  	    protected Integer doInBackground(String... args) {
        	if (username == null || username.indexOf("@") < 1) {
        		state = getString(R.string.ConnectionError);
                sendBroadcast(intent);
                Notify.offlineNotify(state);
        		return 0;
        	} else {
        		SmackConfiguration.setPacketReplyTimeout(60000);
        		SmackConfiguration.setKeepAliveInterval(60000);
            	
        		String host = StringUtils.parseServer(username);
  	        	String user = StringUtils.parseName(username);
  	        	
  	        	if (service == null || service.length() < 4) {
            		try {
            			Lookup lookup = new Lookup("_xmpp-client._tcp." + host, Type.SRV);
            			lookup.setCredibility(Credibility.ANY);
                	    Record[] records = lookup.run();
                	    if(lookup.getResult() == Lookup.SUCCESSFUL) {
                	    	if (records.length > 0) {
                	    		SRVRecord record = (SRVRecord) records[0];
                	    		service = record.getTarget().toString();
                	    		service = service.substring(0, service.length()-1);
                	    		port = record.getPort();
                	    	}
                	    }
            		}
            		catch(TextParseException tpe) { }
            	}
  	        	
            	if (service == null || service.length() <= 3) service = host;
            	
            	ConnectionConfiguration cc = new ConnectionConfiguration(service, port, host);
            	cc.setCapsNode("http://jtalk.ustyugov.net/caps");
            	cc.setSelfSignedCertificateEnabled(true);
            	cc.setReconnectionAllowed(false);
            	cc.setRosterLoadedAtLogin(true);
            	cc.setSendPresence(false);
            	cc.setSASLAuthenticationEnabled(sasl.equals("0") ? false : true);
            	cc.setSecurityMode(tls.equals("0") ? SecurityMode.disabled : SecurityMode.enabled);
            	
//            	} else if (prefs.getBoolean("EnableTls", true) && !prefs.getBoolean("UseCompression", false)) {
//            		cc.setSecurityMode(SecurityMode.enabled);
//            		cc.setCompressionEnabled(false);
//            	} else if (prefs.getBoolean("EnableTls", true) && prefs.getBoolean("UseCompression", false)) {
//            		cc.setSecurityMode(SecurityMode.required);
//            		cc.setCompressionEnabled(true);
//            	}
            	
            	if (service.equals("talk.google.com")) cc.setSASLAuthenticationEnabled(false);
            	else if (service.equals("vkmessenger.com")) cc.setSecurityMode(SecurityMode.disabled);
            	
            	connection = new XMPPConnection(cc);
            	connection.setSoftName(getString(R.string.app_name));
            	connection.setSoftVersion(getString(R.string.version) + " (" + getString(R.string.build) + ")");
            	connection.addFeature("http://jabber.org/protocol/disco#info");
            	connection.addFeature("http://jabber.org/protocol/muc");
            	connection.addFeature("http://jabber.org/protocol/chatstates");
            	connection.addFeature("http://jabber.org/protocol/bytestreams");
            	connection.addFeature("jabber:iq:version");
            	connection.addFeature("urn:xmpp:receipts");
            	connection.addFeature("urn:xmpp:time");
            	connection.addFeature("urn:xmpp:message-correct:0");
  	    	 
            	int priority = prefs.getInt("currentPriority", 0);
  	    	 	String status  = prefs.getString("currentStatus", "");
  	    	 	String mode  = prefs.getString("currentMode", "available");
  	    	 
  	    	 	try {
					if (!connection.isConnected()) connection.connect();
  	    		} catch (XMPPException xe) {
  	    			state = "Error connecting to " + connection.getServiceName();
					sendBroadcast(intent);
					Notify.offlineNotify(state);
//					reconnect();
					return 0;
				}
    
  	    		if (connection != null && connection.isConnected() && !connection.isAuthenticated()) {
  	    			connection.addPacketListener(msgListener, new PacketTypeFilter(Message.class));
  	    			connection.addConnectionListener(conListener);

  	    			try {
  	    				connection.login(user, password, resource);
  	    			} catch (XMPPException e) {
  	    				XMPPError error = e.getXMPPError();
  	    				if (error != null) state = "[" + error.getCode() + "]: " + error.getMessage();
  	    				else state = e.getLocalizedMessage();
    					sendBroadcast(intent);
    					Notify.offlineNotify(state);
    					return 0;
  	    			}

    				try {
    					roster = connection.getRoster();
    					roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
    					Thread.sleep(2000);

    					new PrivacyListManager(connection);
    					new ServiceDiscoveryManager(connection);
    					new AdHocCommandManager(connection);
    					fileTransferManager = new FileTransferManager(connection);
    					fileTransferManager.addFileTransferListener(new IncomingFileListener());
    				
    					sendPresence(status, mode, priority);
    					roster.addRosterListener(new RstListener());
    				} catch(Exception e) { return 0;}
				}
  	         	return 1;
        	}
  	     }

  		@Override
  	    protected void onPostExecute(Integer result) { 
  	    	if (result != null && result == 1 && isAuthenticated()) {
  	    		state = "";
  				Notify.updateNotify();
  				
  				VCard vCard = new VCard();
  				try {
  					vCard.load(connection, StringUtils.parseBareAddress(connection.getUser()));
  				} catch (XMPPException e) {	}
  				setVCard(vCard);
  				
  	  	    	try {
					MultiUserChat.addInvitationListener(connection, new InviteListener());
				} catch (Exception e) { }
  	  	    	
  	  	    	if (!conferencesHash.isEmpty()) {
  	  	    		Collection<Conference> coll = joinedConferences.values();
  	  	  	    	for (Conference conf : coll) {
  	  	  	    		joinRoom(conf.getName(), conf.getNick(), conf.getPassword());
  	  	  	    	}
  	  	    	} else {
  	  	    		if (prefs.getBoolean("AutoJoin", false)) {
  	  	    			try {
  	  	    				BookmarkManager bm = BookmarkManager.getBookmarkManager(connection);
  	  	    				for(BookmarkedConference bc : bm.getBookmarkedConferences()) {
  	  	    					String nick = bc.getNickname();
  	  	    					if (nick == null || nick.length() < 1) nick = StringUtils.parseName(connection.getUser());
  	  	    					if (bc.isAutoJoin()) joinRoom(bc.getJid(), bc.getNickname(), bc.getPassword());
  	  	    				}
  	  	    			} catch (XMPPException e) { }
  	  	    		}
  	  	    	}
  	  	    	
  	  	    	if (prefs.getBoolean("Locations", false)) {
  	  	    		try {
  	  	    		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, Constants.LOCATION_MIN_TIME, Constants.LOCATION_MIN_DIST, locationListener);
  	  	    		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Constants.LOCATION_MIN_TIME, Constants.LOCATION_MIN_DIST, locationListener);
  	  	    		} catch (Exception e) {
  	  	    			Log.e("LOCATION", e.getLocalizedMessage());
  	  	    		}
  	  	    	} else {
  	  	    		sendLocation(null);
  	  	    	}
  	  	    	
  	  	    	OfflineMessageManager omm = new OfflineMessageManager(connection);
  	  	    	try {
  	  	    		omm.deleteMessages();
  	  	    	} catch (XMPPException e) {	}
  	    	}
  	    	
  	    	sendBroadcast(new Intent(Constants.CONNECTION_STATE));
  	    	Intent updateIn = new Intent(Constants.UPDATE);
			sendBroadcast(updateIn);
		
			resetTimer();
			new IgnoreList.CreateIgnoreList().execute();
  	    }
  	 }
}
